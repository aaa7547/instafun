/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.starter;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.util.List;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnKeyListener {
  RelativeLayout relativeLayout;
  ImageView instaFunImageView;
  EditText usernameTextField;
  EditText passwordTextField;
  TextView changeSignUpTextView;
  Button clickSignUpButton;
  Boolean signUpAvailable;

  @Override
  public boolean onKey(View v, int keyCode, KeyEvent event) {
    if(keyCode == KeyEvent.KEYCODE_ENTER) {
      signUpOrLogInButton(v);
    }
    return false;
  }

  @Override
  public void onClick(View v) {
    if(v.getId() == R.id.pleaseChangeSignUp) {
      if(signUpAvailable == true) {
        signUpAvailable = false;
        changeSignUpTextView.setText("Sign Up");
        clickSignUpButton.setText("Log In");
      } else {
        signUpAvailable = true;
        changeSignUpTextView.setText("Log In");
        clickSignUpButton.setText("Sign Up");
      }
    } else if(v.getId() == R.id.instaFunImageView || v.getId() == R.id.relativeLayout) {
      InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
      inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

    }

  }

  public void signUpOrLogInButton(View view) {
    if(signUpAvailable == true) {
      ParseUser parseUser = new ParseUser();
      parseUser.setUsername(String.valueOf(usernameTextField.getText()));
      parseUser.setPassword(String.valueOf(passwordTextField.getText()));

      parseUser.signUpInBackground(new SignUpCallback() {
        @Override
        public void done(ParseException e) {
          if (e == null) {
            Log.i("InstaFunInfo", "Thank you for signing up Insta Fun account!");
            showFriendsList();

          } else {
            Toast.makeText(getApplicationContext(), e.getMessage().substring(e.getMessage().indexOf(" ")), Toast.LENGTH_LONG).show();
          }
        }
      });
    } else{
      ParseUser.logInInBackground(String.valueOf(usernameTextField.getText()), String.valueOf(passwordTextField.getText()), new LogInCallback() {
        @Override
        public void done(ParseUser user, ParseException e) {
         if (user != null) {
           Log.i("InstaFunInfo", "Your login account is successful!!!");
           showFriendsList();
         } else {
           Toast.makeText(getApplicationContext(), e.getMessage().substring(e.getMessage().indexOf(" ")), Toast.LENGTH_LONG).show();
         }
        }
      });
    }


  }

  public void showFriendsList() {
    Intent i = new Intent(getApplicationContext(), FriendsList.class);
    startActivity(i);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    if(ParseUser.getCurrentUser() != null) {
      showFriendsList();
    }

    signUpAvailable = true;
    relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
    instaFunImageView = (ImageView) findViewById(R.id.instaFunImageView);
    usernameTextField = (EditText) findViewById(R.id.username);
    passwordTextField = (EditText) findViewById(R.id.password);
    changeSignUpTextView = (TextView) findViewById(R.id.pleaseChangeSignUp);
    clickSignUpButton = (Button) findViewById(R.id.clickSignUpButton);


    relativeLayout.setOnClickListener(this);
    instaFunImageView.setOnClickListener(this);
    changeSignUpTextView.setOnClickListener(this);
    usernameTextField.setOnKeyListener(this);
    passwordTextField.setOnKeyListener(this);


    ParseAnalytics.trackAppOpenedInBackground(getIntent());
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

}
